﻿using Allotrope.AdfExplorer.Common.Infrastructure;
using com.hp.hpl.jena.query;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Allotrope.EndpointPlugin
{
    internal partial class EndpointPlugin
    {
        private static Dataset _dataset = DatasetFactory.create();
        
        public static Dictionary<string,string> QuerySelect(string queryString, string namedGraph, string[] accepts)
        {
            Console.WriteLine("QueryEngine.QuerySelect: " + queryString +" -> "+ string.Join(",", accepts));
            Query query = QueryFactory.create(queryString);
            QueryExecution qexec;
            if (namedGraph != null) {
                qexec = QueryExecutionFactory.create(query, _dataset.getNamedModel(namedGraph));
            }
            else {
                qexec = QueryExecutionFactory.create(query, _dataset);
            }
            Dictionary<string,string> answer = new Dictionary<string, string>();
            try
            {
                ResultSet results = qexec.execSelect();
                answer = Format(results, accepts);
            }
            finally
            {
                qexec.close();
            }
            Console.WriteLine("QueryEngine.QuerySelect: " + answer["ContentType"] +" -> "+ answer["Body"] );
            return answer;
        }

        public static Dictionary<string,string> Format(ResultSet results, string[] accepts) {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            java.io.ByteArrayOutputStream outputStream = new java.io.ByteArrayOutputStream();
            foreach (string a in accepts)
            {
                string accept = a.IndexOf(";") == 0 ? a : a.Split(';')[0];
                if (accept.Equals("application/sparql-results+json") || accept.Equals("application/json"))
                {
                    dict.Add("ContentType", accept);
                    ResultSetFormatter.outputAsJSON(outputStream, results);
                    break;
                }
                else if (accept.Equals("application/sparql-results+xml") || accept.Equals("application/rdf+xml"))
                {
                    dict.Add("ContentType", accept);
                    ResultSetFormatter.outputAsXML(outputStream, results);
                    break;
                }
                else if (accept.Equals("application/xml")) {
                    // sesame windows client
                    dict.Add("ContentType", accept);
                    ResultSetFormatter.outputAsXML(outputStream, results);
                    break;
                }
                else if (accept.Equals("application/javascript"))
                {
                    // which serialization is expected?
                    dict.Add("ContentType", accept);
                    ResultSetFormatter.outputAsJSON(outputStream, results);
                    break;
                }
                else if (accept.Equals("text/csv"))
                {
                    dict.Add("ContentType", accept);
                    ResultSetFormatter.outputAsCSV(outputStream, results);
                    break;
                }
                else if (accept.Equals("text/tab-separated-values"))
                {
                    dict.Add("ContentType", accept);
                    ResultSetFormatter.outputAsTSV(outputStream, results);
                    break;
                }
            }
            byte[] data = Array.ConvertAll(outputStream.toByteArray(), (a) => (byte)a);
            string answer = System.Text.Encoding.Default.GetString(data);
            dict.Add("Body", answer);
            if (!dict.ContainsKey("ContentType")) {
                dict.Add("ContentType", "application/json");
            }
            return dict;
        }
    }
}
