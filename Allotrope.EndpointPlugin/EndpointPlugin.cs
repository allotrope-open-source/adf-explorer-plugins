﻿using Allotrope.AdfExplorer.Plugins.PluginSdk;
using Allotrope.AdfExplorer.Common.Infrastructure;
using org.allotrope.adf.model;
using org.allotrope.adf.dd.model;

namespace Allotrope.EndpointPlugin
{
    internal partial class EndpointPlugin : BasePlugin
    {
        // these are mandatory plugin properties
        public override string Name => "Endpoint Plugin";
        public override string Author => "johanna.voelker@bayer.com";
        public override string Description => "This plugin opens a SPARQL endpoint.";

        // Topics
        private BusSubscriptionHelper _busHelper;
        public static readonly string[] LoadAdfFile = { SdkConstants.Topics.DAL, SdkConstants.Topics.LOAD_ADF_FILE, SdkConstants.Topics.ASTERISK };
        public static readonly string[] CloseAdfFile = { SdkConstants.Topics.DAL, SdkConstants.Topics.CLOSE_FILE, SdkConstants.Topics.ASTERISK };

        protected override void BusConnect()
        {
            _busHelper = new BusSubscriptionHelper(Bus);
            _busHelper.Subscribe("load ADF file", LoadAdfFile, LoadFile);
            _busHelper.Subscribe("close file", CloseAdfFile, CloseFile);
        }

        protected override void Ready()
        {
            base.Ready();
            OpenEndpoint();
        }

        private void LoadFile(Message msg)
        {
            string file = msg.Data.file;
            string token = msg.Data.token;

            // base.Logger.Debug($"EndpointPlugin: topic={msg.Topic}, file={msg.Data.file}, token={msg.Data.token}");

            var requestClient = new RequestResponse(Bus, BusHelper.ToTopic(SdkConstants.Topics.DAL, SdkConstants.Topics.REQUEST, token),
                                                          BusHelper.ToTopic(SdkConstants.Topics.DAL, SdkConstants.Topics.RESPONSE));

            requestClient.Request<AdfFile, AdfFile>(adfFile => adfFile,
              response =>
              {
                  DataDescription dd = response.Result.getDataDescription();
                  // dd.write(java.lang.System_.@out, "TTL");
                  var uri = new System.Uri(file);
                  _dataset.addNamedModel(uri.AbsoluteUri, dd);
                  _dataset.setDefaultModel(dd);
              });
        }

        private void CloseFile(Message msg) {
            string file = msg.Data.file;
            string token = msg.Data.token;

            var requestClient = new RequestResponse(Bus, BusHelper.ToTopic(SdkConstants.Topics.DAL, SdkConstants.Topics.REQUEST, token),
                                                          BusHelper.ToTopic(SdkConstants.Topics.DAL, SdkConstants.Topics.RESPONSE));

            requestClient.Request<AdfFile, AdfFile>(adfFile => adfFile,
              response =>
              {
                  _dataset.removeNamedModel(file);
              });
        }

        protected override void OnDispose(bool disposing)
        {
            if (disposing)
            {
                _busHelper?.Dispose();
                _server?.Dispose();
            }
        }
    }
}