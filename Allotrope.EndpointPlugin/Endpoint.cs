﻿using Allotrope.AdfExplorer.Common.Infrastructure;
using log4net;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Unosquare.Labs.EmbedIO;
using Unosquare.Labs.EmbedIO.Constants;
using Unosquare.Labs.EmbedIO.Modules;
using Unosquare.Net;

namespace Allotrope.EndpointPlugin
{
    internal partial class EndpointPlugin
    {
        private WebServer _server;
        private static readonly string _endpoint = "http://localhost:9000/";

        public void OpenEndpoint()
        {
            _server = new WebServer(_endpoint, RoutingStrategy.Regex);
            _server.RegisterModule(new WebApiModule());
            _server.Module<WebApiModule>().RegisterController<SparqlController>();
            _server.RunAsync();
        }
    }

    internal class SparqlController : WebApiController
    {
        private void debug(HttpListenerContext context) {
            Console.WriteLine("Endpoint.debug: " + ToString(context));
        }
        private string ToString(HttpListenerContext context) {
            StringBuilder sb = new StringBuilder();

            var verb = Extensions.RequestVerb(context);
            string path = Extensions.RequestPath(context);
            string contentType = context.Request.ContentType;
            string body = Extensions.RequestBody(context);            
            string[] acceptTypes = context.Request.AcceptTypes;

            sb.Append( "request.verb = " + verb +", " );
            sb.Append( "request.path = " + path +", " );
            sb.Append( "request.content-type = " + contentType +", " );
            sb.Append( "request.accept-types = (" + string.Join(",", acceptTypes) + "), ");
            sb.Append( "request.body = " + body );

            return sb.ToString();
        }

        [WebApiHandler(HttpVerbs.Get, "/sparql")]
        public bool GetQuery(WebServer server, HttpListenerContext context)
        {
            debug(context);
            string query = context.Request.QueryString.Get("query");
            string namedGraph = context.Request.QueryString.Get("named-graph-uri");
            // string defaultGraph = context.Request.QueryString.Get("default-graph-uri");
            string[] accepts = context.Request.AcceptTypes;
            if (query == null){
                context.JsonResponse(new { Message = "GET.SPARQL: no query" });
            }
            else {
                Dictionary<string, string> answer = EndpointPlugin.QuerySelect(query, namedGraph, accepts);
                Send(server, context, answer);                                
            }
            return true;
        }
        [WebApiHandler(HttpVerbs.Post, "/sparql")]
        public bool PostQuery(WebServer server, HttpListenerContext context)
        {
            debug(context);
            string body = Extensions.RequestBody(context);            
            string contentType = context.Request.ContentType;
            if (contentType.StartsWith("application/x-www-form-urlencoded"))
            {
                return PostQueryForm(server, context, body);
            }
            else if (contentType.StartsWith("application/sparql-query"))
            {
                return PostQuerySparql(server, context, body);
            }
            return true;
        }

        private void Send(WebServer server, HttpListenerContext context, Dictionary<string,string> data) {
            string body;
            if (data.TryGetValue("Body", out body))
            {
                context.Response.StatusCode = 200;
                context.Response.ContentType = data["ContentType"];
                byte[] buffer = Encoding.UTF8.GetBytes(data["Body"]);
                context.Response.ContentLength64 = buffer.Length;
                System.IO.Stream output = context.Response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
                context.Response.OutputStream.Flush();
                context.Response.OutputStream.Close();
            }
            else {
                context.JsonResponse(new { Message = "Endpoint.Send: no data", Debug = ToString(context) });
            }
        }

        // POST: header=accepts, body=query
        private bool PostQuerySparql(WebServer server, HttpListenerContext context, string body) {
            string[] accepts = context.Request.AcceptTypes;
            string namedGraph = context.Request.QueryString.Get("named-graph-uri");
            // string defaultGraph = context.Request.QueryString.Get("default-graph-uri");
            string query = Decode(body);
            Dictionary<string,string> answer = EndpointPlugin.QuerySelect(query, namedGraph, accepts);
            Send(server, context, answer);
            return true;
        }

        // POST: body=query&header&format
        private bool PostQueryForm(WebServer server, HttpListenerContext context, string body) {
            Dictionary<string, string> dict = Parameters(body);
            string query;
            if (dict.TryGetValue("query", out query))
            {
                string[] accepts = context.Request.AcceptTypes;
                if (accepts.Length == 0) {
                    string format;
                    if (dict.TryGetValue("format", out format)) {
                        accepts = format.Split(';');
                    }
                }
                string namedGraph;
                if (dict.TryGetValue("named-graph-uri", out namedGraph))
                {
                    Dictionary<string, string> answer = EndpointPlugin.QuerySelect(query, namedGraph, accepts);
                    Send(server, context, answer);
                }
                else
                {
                    Dictionary<string, string> answer = EndpointPlugin.QuerySelect(query, null, accepts);
                    Send(server, context, answer);
                }
            }
            else
            {
                context.JsonResponse(new { Message = "POST.SPARQL: no query", Body = body });
            }
            return true;
        }

        private Dictionary<string, string> Parameters(string body) {
            Dictionary<string, string> postParams = new Dictionary<string, string>();
            string[] rawParams = body.Split('&');
            foreach (string param in rawParams)
            {
                string[] kvPair = param.Split('=');
                if (kvPair.Length == 2)
                {
                    string key = kvPair[0];
                    string value = Decode(kvPair[1]);
                    postParams.Add(key, value);
                }
            }
            return postParams;
        }

        private string Decode(string text) {
            string result = Uri.UnescapeDataString(text);
            result = result.Replace('+', ' ');
            return result;
        }
    }
}
