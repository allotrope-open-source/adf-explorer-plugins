﻿using Allotrope.AdfExplorer.Common.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Allotrope.TestPlugin
{
    internal partial class TestPlugin
    {
        private readonly string[] RenderFileContentTopic = 
        {
            SdkConstants.Topics.DAL, SdkConstants.Topics.RENDER_FILE_CONTENT
        };

        private static readonly string[] LoadAdfFileTopic =
        {
            SdkConstants.Topics.DAL, SdkConstants.Topics.LOAD_ADF_FILE, SdkConstants.Topics.ASTERISK
        };
    }
}
