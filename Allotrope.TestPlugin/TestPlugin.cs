﻿using Allotrope.AdfExplorer.Common.Infrastructure;
using Allotrope.AdfExplorer.Plugins.PluginSdk;
using System;

namespace Allotrope.TestPlugin
{
    internal partial class TestPlugin : BasePlugin
    {
        public override string Name => "Test Plugin";
        public override string Author => "johanna.voelker@bayer.com";
        public override string Description => "This is a test plugin.";

        private BusSubscriptionHelper _busHelper;

        protected override void BusConnect()
        {
            _busHelper = new BusSubscriptionHelper(Bus);
            // _busHelper.Subscribe(Constants.LOAD_ADF_FILE, LoadAdfFileTopic, ShowTopic);
            // _busHelper.Subscribe(Constants.RENDER_FILE_CONTENT, RenderFileContentTopic, ShowTopic);
        }

        private void ShowTopic(Message message) {
            base.Logger.Info($"TestPlugin: topic={message.Topic}, file={message.Data.file}, token={message.Data.token}");
        }

        protected override void OnDispose(bool disposing)
        {
            if (disposing)
            {
                _busHelper?.Dispose();
            }
        }
    }
}
