﻿using Allotrope.AdfExplorer.Common.Infrastructure;

namespace Allotrope.TestPlugin
{
    public static class Constants
    {
        public const string LOAD_ADF_FILE = "load adf file";
        public const string RENDER_FILE_CONTENT = "render file content";
    }
}
