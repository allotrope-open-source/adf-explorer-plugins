set PATH="C:\Program Files (x86)\MSBuild\14.0\Bin"

@ECHO ON
rem Build psample plugins projects and create pluging nuget packages
rem Usage build [Configuration [Version]]
rem default configuration is Debug ,default version is project assembly version
SET realNuget=..\..\\Allotrope.AdfExplorer\Dependencies\nuget\nuget.exe

if [%1]==[] goto defaultConfig
SET Configuration=%1
goto setVertion
:defaultConfig
@ECHO Use Debug configuration
SET Configuration=Debug
:setVertion

if [%2]==[] goto defaultVer
SET Version=-version %2
goto build
:defaultVer
SET Version=
:build

REM Clean solution
msbuild Allotrope.SamplePlugins.sln /t:clean /p:configuration=%Configuration%
REM Download required nuget  
%realNuget% restore Allotrope.SamplePlugins.sln 
msbuild Allotrope.SamplePlugins.sln /p:Configuration=%Configuration%

ECHO Create plugin packages:
SET Properties=framework=net461;binOutput=bin;
SET OutputDirectory=PluginPackages
ECHO Nuget Properties: %Properties%
REM Clear package or create output folder
IF EXIST %OutputDirectory% ( 
  DEL /S /F /Q %OutputDirectory%
) ELSE (
  MKDIR %OutputDirectory%
)
rem Create packages
FOR /D  %%p in (Allotrope.*) DO IF EXIST "%%p\*.nuspec" (
 cd %%p
 rem prevent nuget to put dependencies on packages used in project  
 IF EXIST  packages.config (
	MOVE /Y  packages.config packages.config.hideFromNuget
)
  %realNuget% pack -OutputDirectory ..\%OutputDirectory% -properties %Properties% %Version%  
  IF EXIST  packages.config.hideFromNuget (
	MOVE /Y  packages.config.hideFromNuget packages.config
)
  cd..
)


